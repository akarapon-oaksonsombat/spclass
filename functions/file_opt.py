'''
    This module is file opt.
'''
import os
import os.path


def rm_when_lower(filename):
    '''
        Function will remove file.
    '''

    if filename.isupper():
        return
    if os.path.isfile(filename):
        os.remove(filename)
